﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu()]
public class casa : ScriptableObject{
    public List<AreaType> listaAreasType;
}

[System.Serializable]
public class AreaType {
    public int id;
    public string nombre;
    public List<Area> listaArea;
}

[System.Serializable]
public class Area{
	public int id;
	public string urlGameObjectArea;
    public string urlImageArea;
    public List<Marker> listaMarker;
}

[System.Serializable]
public class Marker{
	public int id;
	public string name;
    public List<Textures> listaUrlTexturas;
}

[System.Serializable]
public class Textures{
	public string codigo;
	public string urlTextura;
}