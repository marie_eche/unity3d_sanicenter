﻿using UniRx.Async;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class mainController : MonoBehaviour {

    enum enumAmbientes {Sala, Cocina, Dormitorio, Baño, Terraza}
    public GameObject goPanelCabecera;
	public GameObject goPanelMenuLeft;
	public GameObject goPanelOpcionesCompartir;
	public GameObject goPanelAreaSelected;
    public GameObject goContentMenuTop;
	public GameObject goContentMenuLeft;
	public GameObject goContentPopUpArea;
    public GameObject goButtonMenuPrincipal;
    public GameObject goButtonTipoArea;
    public GameObject goButtonMarker;
	public GameObject goButtonTexture;
	public GameObject goMarkerArea;
	public GameObject goPopUpAreas;
	public Button buttonSearch;
    public Button buttonShare;
    public Button buttonFullScreen;
    public Sprite imagePuntosCambio;
    public casa areasCasa;
    Button[] listaBotonesCabecera;
    Button[] listaOpcionesCompartir;
	static List<Renderer> listRenderer;

    static dataController dcDatosAreas;

    private void Awake(){
        //instanciar data controller
        dcDatosAreas = new dataController("", areasCasa);	
    }

    // Use this for initialization
    void Start () {
        Initialize();
        LlenarMenuPrincipal();
        CargarDefaultImage();
	}

    void Initialize(){
       
		buttonSearch.onClick.AddListener (SearchTexture);
        buttonShare.onClick.AddListener(ShareImage);
        buttonFullScreen.onClick.AddListener(FullScreen);
        listaOpcionesCompartir = goPanelOpcionesCompartir.GetComponentsInChildren<Button>();
		marker.onMarkerPressed += LoadTexturesFor3D;
        setEventOnClickGOCompartir(listaOpcionesCompartir);
    }

    void setEventOnClickGOCompartir(Button[] listaOpcionesCompartir){
        
        for (int i = 0; i < listaOpcionesCompartir.Length; i++)
        {
            // listaOpcionesCompartir[i].onClick.AddListener(CompartirAmbiente(listaOpcionesCompartir[i]));
        }
    }

    void LlenarMenuPrincipal(){
       
        List<AreaType> listAreaType = new List<AreaType>();
        string success = string.Empty; 
        listAreaType = dcDatosAreas.GetListAreaType(ref success);

		int index = 0;
        foreach (AreaType item in listAreaType){
          	GameObject goButtonMenuP = Instantiate(goButtonMenuPrincipal,goContentMenuTop.transform);
			Debug.Log (item.nombre);
            goButtonMenuP.name = item.nombre;
            Button buttonMenu = goButtonMenuP.GetComponentInChildren<Button>();
            buttonMenu.name = item.nombre;
			buttonMenu.GetComponentInChildren<Text> ().text = item.nombre;
			buttonMenu.onClick.AddListener(()=>CargarAreas(buttonMenu, item.id));           
        }

    }

    void CargarDefaultImage(){

    }
    void ShareImage(){
        goPanelOpcionesCompartir.SetActive(true);
    }
    void FullScreen(){

    }
    void CompartirAmbiente(Button buttonCompartir)
    {

        switch (buttonCompartir.name)
        {
            case "Facebook": break;
            case "Instagram": break;
            case "Pintrest": break;

        }
        //Debo suscribirme y de suscribirme al evento
    }
    void CargarAreas(Button buttonMenuTop, int index){

        string success = "";
		Debug.Log ("INDEX " + index);
        List<Area> listaArea = new List<Area>();
        listaArea = dcDatosAreas.GetListArea(index, ref success);
        FillAreasImages(listaArea, index);
        //Poner AnimaclistaAreaión Dot
		goPopUpAreas.SetActive(true);

    }

    void AreaSelected(int index, int indexTipoArea){        
		FillMarkers();
    }

	void FillMarkers(){		
        int indexMarker = 0;
        string success = "";

		goPopUpAreas.SetActive (false);
		Destroy(goPanelAreaSelected.transform.GetChild (0).gameObject);

		GameObject goAreaSelectedPrefab = GetAreaPrefab ();
		GameObject goAreaSelectedPrefabInst = Instantiate (goAreaSelectedPrefab, goPanelAreaSelected.transform);
    }

	void CleanAreaTextures(){

		Button[] buttonTextures = goContentPopUpArea.GetComponentsInChildren<Button> ();
		if (buttonTextures.Length > 0) {
			foreach (var item in buttonTextures) {
				Destroy (item.gameObject);
			}
		}
	}
    
	void FillAreasImages(List<Area> listaArea, int index){
      int indexArea = 0;

	  CleanAreaTextures ();
      foreach (var item in listaArea){
		GameObject goArea =  Instantiate(goButtonTipoArea,goContentPopUpArea.transform);
        Button buttonTipoArea =  goArea.GetComponentInChildren<Button>();
		Debug.Log ("Imagen Area " + item.urlImageArea);
		Image img = goArea.GetComponentInChildren<Image> ();		

		try {
			GetImage (item.urlImageArea).SubscribeWithState<Sprite,Image> (img, (s,i) => {
				i.sprite = s;
			});
		} catch (System.Exception ex) {
			Debug.Log ("FILL AREAS " + ex.Message);
		}

		buttonTipoArea.onClick.AddListener(()=>AreaSelected(index,indexArea++));    
      }  

    }

	void SetListTexturesSprites(List<Textures>listUrlTexture){

		Debug.Log ("Textura nula " + listUrlTexture==null);

		foreach (var item in listUrlTexture) {
			Debug.Log (goButtonTexture.name);
			GameObject goTexture = Instantiate (goButtonTexture.gameObject, goContentMenuLeft.transform);
			Button buttonTexture = goTexture.GetComponentInChildren<Button> ();
			Image img = goTexture.GetComponentInChildren<Image> ();

			Debug.Log ("Textures Sprites");
			GetImage (item.urlTextura).SubscribeWithState2<Sprite,Image, Button> (img, buttonTexture, (s,i,b) => {
				i.sprite = s;
				b.onClick.AddListener (() => ChangeTextureOn3D(s, listRenderer));
			});	
		}
    }

	void CleanButtonsTextures(){

		Button[] buttonTextures = goContentMenuLeft.GetComponentsInChildren<Button> ();
		if (buttonTextures.Length > 0) {
			foreach (var item in buttonTextures) {
				Destroy (item.gameObject);
			}
		}
	}

	void SearchTexture(){
		string success = "";
		string textCodigo =  goPanelMenuLeft.transform.GetChild (1).GetComponentInChildren<InputField> ().text;
		CleanButtonsTextures ();
		if (textCodigo.Trim ()!=string.Empty) {
			List<Textures> listTexturesMarker = new List<Textures>();
			listTexturesMarker = dcDatosAreas.GetTexturesMarker (-1, textCodigo, ref success);
			SetListTexturesSprites (listTexturesMarker);
		}
	}

	void LoadTexturesFor3D(marker _marker){
		goPanelMenuLeft.SetActive (true);
		string success = "";
		CleanButtonsTextures ();
		/*List<Textures> listTexturesMarker = new List<Textures>();
		listTexturesMarker = dcDatosAreas.GetTexturesMarker (_marker.id, "", ref success);*/
		listRenderer = new List<Renderer> ();
		listRenderer = _marker.listRObjects;
		Debug.Log ("Marker ID " + _marker.id);

		foreach (var item in listRenderer) {
			Debug.Log ("Marker" + item.name);	
		}
		//SetListTexturesSprites (listTexturesMarker);
	}

	void ChangeTextureOn3D(Sprite sp, List<Renderer> listGameObject3D){
		Debug.Log ("Change");
		Texture2D text2D = getTextureFromSprite (sp);

		foreach (var item in listGameObject3D) {
			Debug.Log ("Renderer " + item.name);
			item.material.mainTexture = (Texture) text2D;
		}
	}

	Texture2D getTextureFromSprite(Sprite sp){
		
		var _texture2D = new Texture2D((int)sp.rect.width, (int)sp.rect.height);
		var pixels = sp.texture.GetPixels((int)sp.textureRect.x, 
			(int)sp.textureRect.y, 
			(int)sp.textureRect.width, 
			(int)sp.textureRect.height);
		_texture2D.SetPixels( pixels );
		_texture2D.Apply();

		return _texture2D;
	}

	GameObject GetAreaPrefab(){
		return (GameObject)Resources.Load ("Area1");
	}

	IObservable<Sprite> GetImage(string urlImage){
        string ruta = Application.streamingAssetsPath;
        ruta = ruta + "/" + urlImage;

		try {

			Debug.Log("GET IMAGE " + ruta);
			return ObservableWWW.GetAndGetBytes ("file://" + ruta).Select (b => {
				Debug.Log("Observable");
				Texture2D text = new Texture2D(1, 1);
				text.LoadImage(b);
				Sprite sp = Sprite.Create (text, new Rect (0, 0, 128, 128), new Vector2 ());	
				Debug.Log("Texture 2D sprite");
				Debug.Log(text == null);
				return sp;
			}).DoOnError(e=>{
				Debug.Log("Error");
			});
			
		} catch (System.Exception ex) {

			Debug.Log ("Exception " + ex.Message);

		}

		return null;
    }


}
