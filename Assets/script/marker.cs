﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class marker : MonoBehaviour {

	public int id;
	public Button buttonMarker;
	public List<Renderer> listRObjects = new List<Renderer> ();
	public static event markerPresseddelegate onMarkerPressed;
	public delegate void markerPresseddelegate(marker mkr);

	// Use this for initialization
	void Start () {
		buttonMarker.onClick.AddListener (()=>{
			if (onMarkerPressed != null) {
				onMarkerPressed (this);
			}
		});
	}

}
