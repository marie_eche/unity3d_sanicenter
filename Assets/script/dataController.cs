﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dataController {

    casa areasCasa;

    public dataController(string cadena, casa areasCasa){

        this.areasCasa = areasCasa;
    }

    public List<AreaType> GetListAreaType(ref string success){

        success = "";
        return areasCasa.listaAreasType;
    }
    public List<Area> GetListArea(int index, ref string success){
        success = "";
        return areasCasa.listaAreasType[index].listaArea;
    }
    public List<Marker> GetListMarker(int index, int indexArea, ref string success){
        success = "";
        return areasCasa.listaAreasType[index].listaArea[indexArea].listaMarker;
    }
    public List<Textures> GetListMarkerTexture(int index, int indexArea, int indexMarker, ref string success)
    {
        success = "";
        return areasCasa.listaAreasType[index].listaArea[indexArea].listaMarker[indexMarker].listaUrlTexturas;
    }

	public List<Textures> GetTexturesMarker(int id, string code, ref string success){
		success = "";

		if (id == -1) {
			return areasCasa.listaAreasType [0].listaArea [0].listaMarker[int.Parse (code)].listaUrlTexturas;	
		} else {
			return areasCasa.listaAreasType [0].listaArea [0].listaMarker[id].listaUrlTexturas;	
		}

		//List<Marker> listaMarker =  areasCasa.listaAreasType [index].listaArea [indexArea].listaMarker;
		/*int indexMarker = 0;
		foreach (var item in listaMarker) {
			if (item.name == "3D") {
			}
			indexMarker++;
		}*/

		//return null;
	}

}
